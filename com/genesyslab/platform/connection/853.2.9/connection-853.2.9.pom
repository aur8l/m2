<?xml version="1.0" encoding="UTF-8" standalone="no"?><!--
   ===============================================================================
    Genesys Platform SDK
   ===============================================================================

    Any authorized distribution of any copy of this code (including any related
    documentation) must reproduce the following restrictions, disclaimer and copyright
    notice:
  
    The Genesys name, trademarks and/or logo(s) of Genesys shall not be used to name
    (even as a part of another name), endorse and/or promote products derived from
    this code without prior written permission from Genesys Telecommunications
    Laboratories, Inc.
 
    The use, copy, and/or distribution of this code is subject to the terms of the Genesys
    Developer License Agreement.  This code shall not be used, copied, and/or
    distributed under any other license agreement.

    THIS CODE IS PROVIDED BY GENESYS TELECOMMUNICATIONS LABORATORIES, INC.
    ("GENESYS") "AS IS" WITHOUT ANY WARRANTY OF ANY KIND. GENESYS HEREBY
    DISCLAIMS ALL EXPRESS, IMPLIED, OR STATUTORY CONDITIONS, REPRESENTATIONS AND
    WARRANTIES WITH RESPECT TO THIS CODE (OR ANY PART THEREOF), INCLUDING, BUT
    NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
    PARTICULAR PURPOSE OR NON-INFRINGEMENT. GENESYS AND ITS SUPPLIERS SHALL
    NOT BE LIABLE FOR ANY DAMAGE SUFFERED AS A RESULT OF USING THIS CODE. IN NO
    EVENT SHALL GENESYS AND ITS SUPPLIERS BE LIABLE FOR ANY DIRECT, INDIRECT,
    CONSEQUENTIAL, ECONOMIC, INCIDENTAL, OR SPECIAL DAMAGES (INCLUDING, BUT
    NOT LIMITED TO, ANY LOST REVENUES OR PROFITS).
 
    Copyright (c) 2006 - 2017 Genesys Telecommunications Laboratories, Inc. All rights reserved.
   ===============================================================================
--><project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
   <modelVersion>4.0.0</modelVersion>
   <parent>
      <groupId>com.genesyslab.platform</groupId>
      <artifactId>protocols-bom</artifactId>
      <version>853.2.9</version>
      <relativePath>../../../pom.xml</relativePath>
   </parent>
   <artifactId>connection</artifactId>
   <packaging>jar</packaging>
   <name>PSDK :: Commons Connection</name>
   <properties>
      <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
      <doc.dir>${basedir}\..\..\psdk_doc\projects\commons\CommonsProtocolsConnectionJava</doc.dir>
      <doc.dir.sys>${basedir}\..\..\psdk_doc\projects\commons\SystemJava</doc.dir.sys>
      <!-- Empty by default. can be overrided in osgi profile -->
      <manifest.file/>
      <manifest.implementation.title>com.genesyslab.platform.commons.connection</manifest.implementation.title>
      <manifest.component.path>com/genesyslab/platform/commons/protocol/connection</manifest.component.path>
      <classes.for.unit.test>${project.build.outputDirectory}</classes.for.unit.test>
      <!-- 3d-party build versions -->
      <version.log4j>1.3alpha-8</version.log4j>
      <!-- Plugin versions -->
      <version.compiler.plugin>3.0</version.compiler.plugin>
      <version.clean.plugin>2.2</version.clean.plugin>
      <version.jar.plugin>2.2</version.jar.plugin>
      <version.surefire.plugin>2.12.2</version.surefire.plugin>
      <version.emma.plugin>1.2</version.emma.plugin>
      <version.emma4it.plugin>1.3</version.emma4it.plugin>
      <version.dependency.plugin>2.2</version.dependency.plugin>
      <version.resource.plugin>2.5</version.resource.plugin>
      <version.bundle.plugin>2.3.4</version.bundle.plugin>
      <version.properties.plugin>1.0-alpha-2</version.properties.plugin>
      <version.javadoc.plugin>2.7</version.javadoc.plugin>
      <version.cobertura.plugin>2.6</version.cobertura.plugin>
      <version.findbugs.plugin>2.4.0</version.findbugs.plugin>
      <!-- Common dependencies Build versions -->
      <version.testng>5.7</version.testng>
      <version.junit>3.8.1</version.junit>
   </properties>
   <organization>
      <name>Genesys Telecommunication Laboratories, Inc.</name>
      <url>http://www.genesys.com</url>
   </organization>
   <dependencies>
      <dependency>
         <groupId>com.genesyslab.platform</groupId>
         <artifactId>kvlists</artifactId>
         <type>jar</type>
         <scope>compile</scope>
      </dependency>
      <dependency>
         <groupId>com.genesyslab.platform</groupId>
         <artifactId>system</artifactId>
         <type>jar</type>
         <scope>compile</scope>
      </dependency>
      <dependency>
         <groupId>com.genesyslab.platform</groupId>
         <artifactId>commons</artifactId>
         <type>jar</type>
         <scope>compile</scope>
      </dependency>
      <dependency>
         <groupId>io.netty</groupId>
         <artifactId>netty</artifactId>
         <version>3.9.4.Final</version>
      </dependency>
      <dependency>
         <groupId>org.testng</groupId>
         <artifactId>testng</artifactId>
         <version>${version.testng}</version>
         <type>jar</type>
         <classifier>jdk15</classifier>
         <scope>test</scope>
      </dependency>
      <dependency>
         <groupId>junit</groupId>
         <artifactId>junit</artifactId>
         <version>${version.junit}</version>
         <scope>test</scope>
      </dependency>
   </dependencies>
   <build>
      <finalName>connection</finalName>
      <plugins>
         <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-compiler-plugin</artifactId>
            <version>${version.compiler.plugin}</version>
            <configuration>
               <source>1.6</source>
               <target>1.6</target>
               <optimize>true</optimize>
               <showDeprecation>false</showDeprecation>
               <showWarnings>true</showWarnings>
            </configuration>
         </plugin>
         <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-surefire-plugin</artifactId>
            <version>${version.surefire.plugin}</version>
            <configuration>
               <classesDirectory>${classes.for.unit.test}</classesDirectory>
            </configuration>
         </plugin>
         <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-jar-plugin</artifactId>
            <version>${version.jar.plugin}</version>
            <executions>
               <execution>
                  <id>default-jar</id>
                  <goals>
                     <goal>jar</goal>
                  </goals>
                  <configuration>
                     <forceCreation>true</forceCreation>
                     <archive>
                        <manifestFile>${manifest.file}</manifestFile>
                        <manifestEntries>
                           <Implementation-Title>${manifest.implementation.title}</Implementation-Title>
                           <Implementation-Version>${GENESYS.PROJECT.VERSION}</Implementation-Version>
                           <Implementation-Vendor>${project.organization.name}</Implementation-Vendor>
                           <Specification-Title>${project.name}</Specification-Title>
                           <Specification-Version>${GENESYS.PROJECT.VERSION}</Specification-Version>
                           <Specification-Vendor>${project.organization.name}</Specification-Vendor>
                           <Comments>${BUILDTYPE}-${env.COMPUTERNAME}-${maven.build.timestamp}</Comments>
                        </manifestEntries>
                        <manifestSections>
                           <manifestSection>
                              <name>${manifest.component.path}</name>
                           </manifestSection>
                        </manifestSections>
                     </archive>
                  </configuration>
               </execution>
            </executions>
         </plugin>
      </plugins>
   </build>
   <profiles>
      <profile>
         <id>resultpath-copy</id>
         <build>
            <plugins>
               <plugin>
                  <artifactId>maven-antrun-plugin</artifactId>
                  <executions>
                     <execution>
                        <phase>install</phase>
                        <configuration>
                           <tasks>
                              <echo>genesys.resultpath:${genesys.resultpath}</echo>
                              <echo>project.build.directory:${project.build.directory}</echo>
                              <echo>settings.localRepository:${settings.localRepository}</echo>
                              <copy todir="${genesys.resultpath}/release">
                                 <fileset dir="${project.build.directory}">
                                    <include name="${project.artifactId}.jar"/>
                                 </fileset>
                              </copy>
                              <copy flatten="true" todir="${genesys.resultpath}/release/maven/${project.artifactId}">
                                 <fileset dir="${settings.localRepository}/com/genesyslab/platform/${project.artifactId}">
                                    <include name="**/${project.artifactId}-${project.version}.jar"/>
                                    <include name="**/${project.artifactId}-${project.version}-sources.jar"/>
                                    <include name="**/${project.artifactId}-${project.version}.pom"/>
                                    <include name="**/${project.artifactId}-${project.version}-xsd.zip"/>
                                 </fileset>
                              </copy>
                           </tasks>
                        </configuration>
                        <goals>
                           <goal>run</goal>
                        </goals>
                     </execution>
                  </executions>
               </plugin>
            </plugins>
         </build>
      </profile>
      <profile>
         <id>add-src</id>
         <build>
            <plugins>
               <plugin>
                  <groupId>org.apache.maven.plugins</groupId>
                  <artifactId>maven-source-plugin</artifactId>
                  <version>2.1.2</version>
                  <executions>
                     <execution>
                        <id>attach-sources</id>
                        <goals>
                           <goal>jar-no-fork</goal>
                        </goals>
                     </execution>
                  </executions>
               </plugin>
            </plugins>
         </build>
      </profile>
      <profile>
         <id>osgi</id>
         <properties>
            <manifest.file>${project.basedir}/target/classes/META-INF/MANIFEST.MF</manifest.file>
         </properties>
         <build>
            <plugins>
               <plugin>
                  <groupId>org.apache.maven.plugins</groupId>
                  <artifactId>maven-compiler-plugin</artifactId>
               </plugin>
               <plugin>
                  <groupId>org.apache.felix</groupId>
                  <artifactId>maven-bundle-plugin</artifactId>
                  <version>2.3.4</version>
                  <extensions>true</extensions>
                  <executions>
                     <execution>
                        <id>bundle-manifest</id>
                        <phase>process-classes</phase>
                        <goals>
                           <goal>manifest</goal>
                        </goals>
                     </execution>
                  </executions>
                  <configuration>
                     <instructions>
                        <Bundle-Name>Platform SDK Commons.Connection</Bundle-Name>
                        <Export-Package>
                           com.genesyslab.platform.commons.connection;version=${project.version};-noimport:=true,
                           com.genesyslab.platform.commons.connection.configuration;version=${project.version};-noimport:=true,
                           com.genesyslab.platform.commons.connection.resolver;version=${project.version};-noimport:=true,
                           com.genesyslab.platform.commons.connection.interceptor;version=${project.version};-noimport:=true,
                           com.genesyslab.platform.commons.connection.tls;version=${project.version};-noimport:=true,
                           com.genesyslab.platform.commons.connection.impl;version=${project.version};-noimport:=true,
                           com.genesyslab.platform.commons.connection.impl.netty;version=${project.version};-noimport:=true,
                           com.genesyslab.platform.commons.connection.impl.tls;version=${project.version};-noimport:=true,
                           com.genesyslab.platform.commons.connection.impl.xml;version=${project.version};-noimport:=true 
                        </Export-Package>
                        <Import-Package>
                           com.genesyslab.platform.commons;version="[${@},${@}]",
                           com.genesyslab.platform.commons.*;version="[${@},${@}]",
                           org.jboss.netty.*;version="[${@},${version;+;${@}})";resolution:=optional,
                           * 
                        </Import-Package>
                     </instructions>
                  </configuration>
               </plugin>
            </plugins>
         </build>
      </profile>
      <profile>
         <id>doc</id>
         <activation>
            <activeByDefault>false</activeByDefault>
         </activation>
         <build>
            <plugins>
               <!-- read doc.properties file, prepared by doc team -->
               <plugin>
                  <groupId>org.codehaus.mojo</groupId>
                  <artifactId>properties-maven-plugin</artifactId>
                  <version>${version.properties.plugin}</version>
                  <executions>
                     <execution>
                        <goals>
                           <goal>read-project-properties</goal>
                        </goals>
                        <configuration>
                           <files>
                              <file>${doc.dir}/doc.properties</file>
                           </files>
                        </configuration>
                        <phase>prepare-package</phase>
                     </execution>
                  </executions>
               </plugin>
               <!--copy psdk_doc content to project's javadoc directory -->
               <plugin>
                  <artifactId>maven-resources-plugin</artifactId>
                  <executions>
                     <execution>
                        <id>copy-resources</id>
                        <phase>process-resources</phase>
                        <goals>
                           <goal>copy-resources</goal>
                        </goals>
                        <configuration>
                           <outputDirectory>${basedir}/src/main/javadoc</outputDirectory>
                           <resources>
                              <resource>
                                 <directory>${doc.dir.sys}</directory>
                                 <filtering>false</filtering>
                              </resource>
                              <resource>
                                 <directory>${doc.dir}</directory>
                                 <filtering>false</filtering>
                              </resource>
                           </resources>
                        </configuration>
                     </execution>
                  </executions>
               </plugin>
               <plugin>
                  <groupId>org.apache.maven.plugins</groupId>
                  <artifactId>maven-javadoc-plugin</artifactId>
                  <version>${version.javadoc.plugin}</version>
                  <executions>
                     <execution>
                        <goals>
                           <goal>jar</goal>
                        </goals>
                     </execution>
                  </executions>
                  <configuration>
                     <!--outputDirectory>${project.build.directory}/doc/api</outputDirectory -->
                     <docfilessubdirs>true</docfilessubdirs>
                     <author>true</author>
                     <nodeprecated>false</nodeprecated>
                     <nodeprecatedlist>false</nodeprecatedlist>
                     <notree>false</notree>
                     <nonavbar>false</nonavbar>
                     <noindex>false</noindex>
                     <show>public</show>
                     <source>1.6</source>
                     <splitindex>true</splitindex>
                     <use>true</use>
                     <version>true</version>
                     <quiet>true</quiet>
                     <sourcepath> ${basedir}\src\main\java;
                        ${basedir}\..\SystemJava\system\src\main\java;
                        ${basedir}\..\SystemJava\logging\src\main\java 
                     </sourcepath>
                     <excludePackageNames> *.runtime; *.impl;
                        *.interceptor; *.timer; *.processing; *.xml; 
                     </excludePackageNames>
                     <doctitle>${text.doc.title}</doctitle>
                     <header>${text.connection.doc.header}</header>
                     <footer>${text.connection.doc.footer}</footer>
                     <bottom>${text.doc.bottom}</bottom>
                  </configuration>
               </plugin>
            </plugins>
         </build>
      </profile>
   </profiles>
   <reporting>
      <plugins>
         <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-project-info-reports-plugin</artifactId>
            <version>2.4</version>
         </plugin>
         <plugin>
            <groupId>org.codehaus.mojo</groupId>
            <artifactId>cobertura-maven-plugin</artifactId>
            <version>${version.cobertura.plugin}</version>
         </plugin>
      </plugins>
   </reporting>
   <distributionManagement>
      <repository>
         <uniqueVersion>false</uniqueVersion>
         <id>${prod.distribution.repository.id}</id>
         <name>${prod.distribution.repository.name}</name>
         <url>${prod.distribution.repository.url}</url>
      </repository>
      <snapshotRepository>
         <uniqueVersion>false</uniqueVersion>
         <id>${distribution.repository.id}</id>
         <name>${distribution.repository.name}</name>
         <url>${distribution.repository.url}</url>
      </snapshotRepository>
   </distributionManagement>
</project>